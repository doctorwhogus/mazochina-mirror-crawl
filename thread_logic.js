const { threadQueue } = require('./queue');
const { query } = require('./db');
const { crawlThreadPage } = require('./crawl');
const throttle = require('./global_throttle');
const { now } = require('./time_utils');
const { toSc } = require('./chinese');

module.exports.doNext = async threadId => {
    const meta = (await query('SELECT forum_id, dirty_until_page, most_recent_update, last_page_number FROM thread_targets WHERE thread_id = $1', [threadId])).rows[0];
    const pageToCrawl = meta.dirty_until_page + 1;
    const lock = await throttle();
    const refTime = now();
    let result;
    try {
        result = await crawlThreadPage(meta.forum_id, threadId, pageToCrawl);
    } catch (e) {
        if (e && e.message === 'TOPIC_NOT_EXIST') {
            lock.success();
            await query('UPDATE thread_targets SET successive_fail = -1000 WHERE thread_id = $1', [threadId]);
        } else if (e && e.message === 'TOPIC_NOT_AUTHORISED') {
            lock.success();
            await query('UPDATE thread_targets SET successive_fail = -2000 WHERE thread_id = $1', [threadId]);
        } else {
            lock.fail();
            await query('UPDATE thread_targets SET successive_fail = successive_fail + 1 WHERE thread_id = $1', [threadId]);
            threadQueue.push(threadId);
        }
        return;
    }
    lock.success();
    const queries = [];
    queries.push(query('UPDATE thread_targets SET successive_fail = 0, last_success_crawl = $1, thread_title = $2, thread_title_sc = $3, forum_id = $4, poll_title = $5 WHERE thread_id = $6', [refTime, result.title, toSc(result.title), result.forumId, result.poll || '', threadId]));
    result.posts.forEach(post =>
        queries.push(query('INSERT INTO posts (post_id, thread_id, title, user_name, content, posted_date, most_recent_update, content_sc) VALUES ($1, $2, $3, $4, $5, $6, $7, $8) ON CONFLICT (post_id) DO ' + (post.content.trim().length ? 'UPDATE SET thread_id = EXCLUDED.thread_id, title = EXCLUDED.title, user_name = EXCLUDED.user_name, content = EXCLUDED.content, posted_date = EXCLUDED.posted_date, most_recent_update = EXCLUDED.most_recent_update' : 'NOTHING'), [post.id, threadId, post.title, post.username, post.content, post.date, refTime, toSc(post.content)]))
    );
    await Promise.all(queries);
    if (result.page <= pageToCrawl) { // already last page
        await query('UPDATE thread_targets SET most_recent_update = $1, dirty_most_recent_update = 0, dirty_until_page = 0, last_page_number = $2 WHERE thread_id = $3', [result.posts[result.posts.length - 1].date, result.page, threadId]);
        return;
    }
    let nextPage = pageToCrawl; // go to next page by default
    if ((pageToCrawl === 1) && (meta.last_page_number > 1)) {
        nextPage = meta.last_page_number - 1; // skip unchanged pages, head to hot area
    } else if ((result.posts[0].date > meta.most_recent_update) && (pageToCrawl <= meta.last_page_number)) { // only happens when some post are deleted, so that the following posts moved to previous pages
        nextPage = pageToCrawl - 2;
        await query('UPDATE thread_targets SET last_page_number = $1 WHERE thread_id = $2', [meta.last_page_number - 1, threadId]);
    }
    await query('UPDATE thread_targets SET dirty_until_page = $1 WHERE thread_id = $2', [nextPage, threadId]);
    threadQueue.push(threadId);
};