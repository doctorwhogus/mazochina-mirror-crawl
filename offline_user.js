const assert = require('assert');
const Crawler = require('crawler');
const logger = require('./logger');
const { dateStrToTsInS } = require('./time_utils');
const { query } = require('./db');
const throttle = require('./global_throttle');

const crawler = new Crawler({
    retries: 0
});

const assertTz = $ => {
    const timeZone = $('#nav-footer > li.rightside > span').text();
    assert(timeZone === 'UTC');
};

const crawlMemberPage = (page/* :int */) => new Promise((success, failed) => {
    crawler.direct({
        uri: 'http://forum.mazochina.com/memberlist.php?start=' + String((page - 1) * 30),
        headers: {
            'Cookie': 'phpbb3_kx28n_u=73187; phpbb3_kx28n_k=; phpbb3_kx28n_u=73187; phpbb3_kx28n_sid=0ff7856c21f6410279cd4d86efa0e820',
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36'
        },
        timeout: 15000,
        callback: (error, res) => {
            if (error) {
                logger.fail({ page }, 'transport', String(error));
                failed();
            } else if (res.statusCode !== 200) {
                logger.fail({ page }, 'http', String(res.statusCode));
                failed();
            } else {
                assertTz(res.$);
                const members = res.$('table#memberlist > tbody > tr');
                if (!members.length) {
                    logger.fail({ page }, 'content', $('.panel .inner').text().trim().replace(/\s+/g, ' '));
                    failed();
                } else {
                    logger.info({ page }, 'success', '');
                    success({
                        members: members.toArray().map(el => {
                            const aun = res.$('a.username', el);
                            assert(aun.length === 1);
                            const matched = aun.attr('href').match(/memberlist\.php\?mode=viewprofile&u=(\d+)$/);
                            assert(matched);
                            const date = res.$('td', el).last();
                            return {
                                username: aun.text(),
                                id: parseInt(matched[1]),
                                date: dateStrToTsInS(date.contents().last().text())
                            };
                        })
                    });
                }
            }
        }
    });
});

const main = async () => {
    for (let i = 1895; i <= 1905; i++) {
        let resp;
        const lock = await throttle();
        try {
            resp = await crawlMemberPage(i);
        } catch (e) {
            lock.fail();
            i--;
            continue;
        }
        lock.success();
        await Promise.all(resp.members.map(({ id, username, date }) => query('INSERT INTO users (user_id, user_name, registered_at) VALUES ($1, $2, $3) ON CONFLICT (user_id) DO NOTHING', [id, username, date])));
        console.log(`From page ${i}, ${resp.members.length} result get, id from ${resp.members[0].id} thru ${resp.members[resp.members.length - 1].id}`);
    }
};

main().catch(e => console.error(e));
