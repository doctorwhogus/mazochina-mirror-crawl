const logger = require('./logger');

class TaskQueue/* <T> */ {

    constructor() {
        this.standardTasks = new Set();
        this.lowPriorityTasks = new Set();
    };

    pop(highOnly/* :bool? */)/* :T? */ {
        let val = this.standardTasks.values().next().value;
        if (val) {
            logger.log('pop-high', val);
            this.standardTasks.delete(val);
        } else if (!highOnly) {
            val = this.lowPriorityTasks.values().next().value;
            if (val) {
                logger.log('pop-low', val);
                this.lowPriorityTasks.delete(val);
            }
        }
        return val;
    };

    push(el/* :T */) {
        if (this.lowPriorityTasks.has(el)) { this.lowPriorityTasks.delete(el); }
        this.standardTasks.add(el);
    };

    pushLow(el/* :T */) {
        if (this.standardTasks.has(el)) { return; }
        this.lowPriorityTasks.add(el);
    };

}

module.exports.forumQueue = new TaskQueue();
module.exports.threadQueue = new TaskQueue();