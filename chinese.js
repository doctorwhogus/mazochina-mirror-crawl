const OpenCC = require('opencc');
const OCC_SC = new OpenCC('t2s.json');

module.exports.toSc = text => {
    return OCC_SC.convertSync(text.toLowerCase());
};