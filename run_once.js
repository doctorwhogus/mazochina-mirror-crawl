const db = require('./db');
const toSc = require('./chinese').toSc;

const runRange = async (pidLow, pidHigh) => {
    const posts = db.query('SELECT post_id, content FROM posts WHERE post_id >= $1 AND post_id < $2', [pidLow, pidHigh]);
    const rows = (await posts).rows;
    const updates = rows.map(row => {
        const newContent = row.content.replace(/&#x([0-9A-F]{1,4});/g, (original, quad) => {
            const charCode = parseInt(quad, 16);
            return String.fromCharCode(charCode);
        });
        const sc = toSc(newContent);
        return db.query('UPDATE posts SET content = $1, content_sc = $2 WHERE post_id = $3', [newContent, sc, row.post_id]);
    });
    await Promise.all(updates);
    return rows.length;
};

const megaRun = async () => {
    for (let pid = 0; pid <= 282000; pid += 100) {
        const updated = await runRange(pid, pid + 100);
        console.log('For ' + pid + ' to ' + (pid + 100));
        console.log('  updated ' + updated + ' rows');
    }
};

megaRun().catch(e => {
    console.error(e);
    throw e;
});